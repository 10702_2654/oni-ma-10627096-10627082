package sample;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class Controller implements Initializable{

    public ComboBox<String> b1;
    public ComboBox <String>b2;
    public ComboBox <String>b3;
    public ComboBox <String>b4;
    public ComboBox <String>b5;
    public ComboBox <String>b6;
    public ComboBox <String>b7;
    public Button btnOk;
    public Label prize;
    public Label prize1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        List<String> strings = new ArrayList<>();

        strings.add("0");
        strings.add("1");
        strings.add("2");
        strings.add("3");
        strings.add("4");
        strings.add("5");

        b1.setItems(FXCollections.observableList(strings));
        b2.setItems(FXCollections.observableList(strings));
        b3.setItems(FXCollections.observableList(strings));
        b4.setItems(FXCollections.observableList(strings));
        b5.setItems(FXCollections.observableList(strings));
        b6.setItems(FXCollections.observableList(strings));
        b7.setItems(FXCollections.observableList(strings));


    }



    public void doSum(ActionEvent actionEvent) {
        int a = Integer.parseInt(b1.getSelectionModel().getSelectedItem());

        prize.setText(String.valueOf(a*70));
        int b = Integer.parseInt(b2.getSelectionModel().getSelectedItem());

        prize.setText(String.valueOf(b*60));
        int c = Integer.parseInt(b3.getSelectionModel().getSelectedItem());

        prize.setText(String.valueOf(c*30));
        int d = Integer.parseInt(b4.getSelectionModel().getSelectedItem());
        prize.setText(String.valueOf(d*30));
        int e = Integer.parseInt(b5.getSelectionModel().getSelectedItem());
        prize.setText(String.valueOf(e*30));
        int f = Integer.parseInt(b6.getSelectionModel().getSelectedItem());
        prize.setText(String.valueOf(f*30));
        int g = Integer.parseInt(b7.getSelectionModel().getSelectedItem());
        prize.setText(String.valueOf(g*25));
        prize.setText("總價格:");
        prize1.setText(String.valueOf(c*30+a*70+b*60+d*30+e*30+f*30+g*25));

    }
}
